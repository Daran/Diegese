# Sombrelinceul

Sombrelincuel est un jeune conté berceau de la nouvel religion du Voile.
La côte ouest est en pleine expansion grâce à ses routes commerciales maritimes.
Le côté est s'est appauvri suite à la fermeture des routes commerciales passant par les montagnes.
Haut lieu mystique, le conté compte une forêt druidique et l'Eglise principale de la Religion du Voile.

## Hermellin

Petit bourgade, épicentre de la religion du Voile dirigée par le Haut Prêtre Fractal.
Vile qui s'ouvre au commerce suite à une longue période de crise.
Cernée par une forêt druidique au nord et des montagnes à l'est,
la ville est coupée du monde mais entame de lourds travaux d'ouverture.

### DESCRIPTION

LM petite bourgade
loi +1
; corruption -3
; societe -2
; folklore 2
; incantation 8
; toute religion dont l’alignement s’éloigne de plus d’un cran de celui de la religion officielle de la ville est, au mieux, malvenue, au pire interdite. Les fidèles qui affichent ouvertement leur affiliation à une religion interdite doivent payer 150% du prix normal pour toute marchandise et tout service et essuyer des moqueries, des insultes ou même des démonstrations de violence.

Fanatique
Danger +0

### DÉMOGRAPHIE

Gouvernement magiocratie (pieuse, Lieu saint)
Population 1 300 (1 200 humains, 50 elfes, 30 nains, 10 halfelins, 1 gnome, 9 autres)
Personnalités :
* Fractal, Archiprêtre de Makrag
* Brutal, Ministre de la Défense
* //perso de samael dont j ai zappé le nom

### PLACE DU MARCHÉ

Valeur de base 1 200 po ; Limite d’achat 6 000 po ; 'Incantation 7 ; Objet faible 3d4, intermédiaire 1d6 ; puissant —

## Istos

## Arton

## X

## Y