# Perce-Nuage

Un vieux dirigeable de guerre transformé et modifié. Il possède de nombreuses caches.

![image-10.png](./image-10.png)

(Inspiré de l'équipage du Serenity de Firefly)

## Personnage

| Illustration | Nom | Titre | Race | Description | PNJ | Niveau |
|---|---|---|-----------------|-----------|---|---|
| <img width="300" src="./image-9.png"> | Tharryc Kazazian | Capitaine du Perce-Nuage | Humain | Il a combattu dans le camp indépendantiste durant la guerre du sud et garde une profonde rancœur contre l'Empire à la suite de leur défaite ; hostile à toute autre autorité que la sienne, c'est un bon commandant qui apprécie son équipe, même s'il peine à le montrer ouvertement. | [Capitaine](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Mercenaires.ashx#MERCENAIREARME) | 8 |
| <img width="300" src="./image-8.png"> | Guéra Korol | Second | Humaine | Camarade de Tharryc durant la guerre, elle l'accompagne désormais dans ses missions les plus dangereuses, emportant généralement une Vorpale et une armure de plate. C'est une guerrière puissante. Elle est mariée à Tudin Korol. | [Palafdin](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Crois%c3%a9s.ashx#GUERRIERSAINT) | 7 |
| <img width="300" src="./image-4.png"> | Tudin Korol | Pilote | Nain | Conservant son sens de l'humour même dans les cas critiques, son habileté a tiré l'équipage de plus d'une situation difficile. Il est marié à Guéra Korol. | [Sniper](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-4/dwarf-ranger-2-rogue-3/) | 5 |
| <img width="300" src="./image-7.png"> | Maurena Braharin | Courtisane Impériale | Humaine? | Élégante et raffinée, elle choisit, accompagne, et initie, les personnes qui font appel à elle ; elle possède une navette personnelle, amarrée au zeppelin, qui lui permet de mener ses affaires de façon autonome. | [Courtisane](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Devins.ashx#DISEUSEDEBONNEAVENTURE) | 6 |
| <img width="300" src="./image-3.png"> | Kweel Linage | Mécanicienne | Humaine | Capable de faire des miracles, même quand il lui manque des pièces de rechange, et même sur un vaisseau aussi vieux que le Perce-Nuage ; son bel optimisme réchauffe l'humeur des autres membres de l'équipage. | [Artificer](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-5/jherek-oivos/) | 6 |
| <img width="300" src="./image.png"> | Silen Eldin | Apprenti | Demi-Elfe | Travail avec Kweel. Iel fait ses premiers pas dans le métier. | [Matelot](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Marins.ashx#MATELOT) | 2 |
| <img width="300" src="./image-2.png"> | Scard Proln | Combattant | Demi-orc | Grand, musclé, aimant les armes et l'argent ; il s'avère cependant loin d'être aussi primaire qu'on pourrait le croire. Il a des connaissances dans bien des lieux. | [Bandit](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Brigands.ashx#BANDITDEGRANDCHEMIN) | 7 |
| <img width="300" src="./image-6.png"> | Allem Glor | Prêtre | Demi-elfe | Cet homme de foi semble chercher à apaiser quelque passé mystérieux et probablement mouvementé. L'unique chose transparente chez Allem sont ses lunettes et ce sont des fausses. | [Chirurgien](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-6/vivisectionist-cleric-human-cleric-7/) | 7 |
| <img width="300" src="./image-5.png"> | Maher Silem | Magicien | Humain | Frère de Lumme ; après des études impeccablement menées, il a renoncé à sa carrière, qui s'annonçait très brillante, pour suivre et soigner sa sœur, qu'il admire profondément. | [Mage](https://www.d20pfsrd.com/bestiary/npc-s/npcs-cr-2/human-wizard-3/) | 3
| <img width="300" src="./image-1.png"> | Lumme Silem | Oracle | Humaine | Sœur de Maher ;  dès son plus jeune âge, elle montre de prodigieuses facultés intellectuelles ; elle est donc envoyée dans une école liée a l'inquisition, « l'Amcadémie » ; en réalité, cet endroit s'avère être un centre de recherche où elle est soumise à d'impitoyables expérimentations, qui certes développeront ses capacités physiques et psychiques, mais la traumatiseront profondément et rendront son caractère durablement instable http://paizo.com/pathfinderRPG/prd/gameMasteryGuide/npcs/seers.html#medium | [Medium](https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.PNJ%20Devins.ashx#MEDIUM) | 5 |

## Activité

Contrebande essentiellement, mais aussi mission d'escortage. Ils connaissent toutes les routes aériennes les plus discrète pour se rendre du nord au sud rapidement sans faire usage de magie.


Source: https://en.wikipedia.org/wiki/Firefly_(TV_series)
