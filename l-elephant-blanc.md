# L'Eléphant Blanc

![](https://cdnb.artstation.com/p/assets/images/images/027/622/031/medium/maxim-nikiforov-tavern-sm.jpg?1592064347)

La brasserie des deux Pointes est située entre le Lokthor et La pointe du col de Karam. Logée à flan de montagne, elle accueille les montagnards et les pélèrins sur la route de l'Abîme de Glace.

Le voyageur peut y trouver de quoi dormir, manger et boire. Il trouvera surtout des bières, spécialités de la maison.

## Gérants

La cuisine est celle du Chef Heldrim. Les bières sont brassées, signées et servies au comptoir par la brasseuse Ilgrid.

<!--
| Ilgrid | Heldrim |
|---|---|
| ![Ilgrid](https://cdna.artstation.com/p/assets/images/images/016/300/060/medium/gareth-sleightholme-img-20190303-160044-994.jpg?1551659618) | ![Heldrim](https://i.pinimg.com/originals/3e/8a/74/3e8a74efb7a023579312e51be6127315.jpg) |


-->

## Carte

### Bières

![](https://cdnb.artstation.com/p/assets/images/images/010/001/871/medium/miguel-gallardo-11.jpg?1522036008)

#### La Blanche Ivoire

Née de l’équilibre du houblon et de l’orge malté, elle est légère et peu alcoolisée (4% vol).

Brassée avec des plantes de montagne, vous retrouverez en bouche un arôme fleurie.

#### La Triple Trompe

Cette bière blonde, pur malt, est à la fois rafraîchissante et forte (7,5% vol), elle offre des saveurs raffinées de caramel, de raisin sec et des arômes de miel.

#### L'IPA Chyderme

Explosion de saveur, avec un final doux et légèrement amer qui ne vous laissera pas sur votre soif (5% vol) ! Une bière fortement houblonnées afin d’améliorer sa conservation le temps du voyage.

#### La Mastodombre

Une bière brune brassée à partir d'un moût caractérisé par sa teneur en grains hautement torréfiés. La présence de ces grains bien grillés dans la recette confère la couleur foncée à la bière ainsi qu'un goût de café ou de cacao. C'est la plus forte des bières de la taverne (9% vol).

Les voyageurs la consomment parfois chaude.

#### La Cimetière des cimes

Le cuve où les fonds de bière se rendent d’eux-mêmes pour croupir. Un mélange mystérieux et mouvant qui varient selon le temps.

### Bouffe

#### Viande du jour au miel et à la Triple Trompe

Selon arrivage servi avec patates et fromage fondu

#### Poisson du jour à la Blanche Ivoire

Selon arrivage servi avec patates et éventuellement légumes de saisons

#### Welff Traditionnel

Originaire du Northvald, le Welff est un plat typique proposé dans les meilleurs restaurants. Cette recette traditionnelle est constitué d'une tranche de pain garnie de jambon et cuite dans un plat à gratin, nappée d’une sauce au fromage fondu, à la Mastodombre et à la moutarde. Ce plat est proposé avec des pommes de terre fries dans l'huile.

![bon ok c'est un welsh](https://static.750g.com/images/1200-630/71aa76e34929d06dd92ec3170d61c507/welsh-img-7303small.jpg)

#### Triple Welff du Chef

Le triple Welff du Chef est un Welff traditionnel composé de trois tranches de pain et de trois tranches de jambon le tout surmonté d’un œuf au plat. Ce plat est proposé avec des pommes de terre fries dans l'huile. Délicieusement gourmande, cette recette montagnarde est réconfortante et palpitante !

#### Potage

Selon arrivage. Composé de patates et légumes de saisons
