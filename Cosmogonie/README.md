# Cosmogonie

## Le chaos primordial

Avant tout, fut Chaos. Chaos est ce lieu inexpliquable où chaque chose existe sans être. Ce rien gigantesque est composé de tout.
Ici se trouve cette amas à jamais inébranlable, résidence de tous ce qui est et n'est pas.

*source*:
- La Théogonie, Hésiode

## L'Épiphanie

Au sein de Chaos naquit Ao en prennant consience de lui même. Ao se pensa, se définit et exista. Il se façonna de tout et se créea. Il y avait Chaos et Lui. Cerné de Chaos, mais séparé de Lui. 

*source*:
- Le Cycle des Princes d'Ambre, Roger Zelazny

## La Genèse

D'un geste, il repoussa le chaos, mais ce dernier ne supportant le vide se rapprocha. Ao combla le vide avec ces songes, ces pensées et son imaginaire. L'espace devint Astral. Du Chaos, Ao extirpa des éléments et les tria. Ainsi fut conçu Air, Eau, Terre et Feu. Là où il se trouvait, Ao se saisit de flamme et les fit tourner en une sphere parfaite. Autour de celle-ci, il enveloppa un manteau de roche tout en soufflant dessus pour la refroidir. Ensuite il versa de l'eau et créa les océans.

- Ajout du plan féerique, matériel(il lui faut un nom), etc
- Creation du dieu permettant l'effacement par renvoi dans le chaos

## Le premier âge

- Premiers dieux
- Premiers titans
- Premières races

## Le second âge

- Secondes races
-A partir de quelle moment on passe sur histoire?