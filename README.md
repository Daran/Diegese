# Présentation

## Introduction.

### Où suis-je?

Tu es actuellement sur une plateforme en ligne d'édition de contenu en groupe. En gros, ici nous pouvons tous participer à la modification de document. Cet outil est très puissant. Nous nous contenterons d'utiliser ce dont nous avons besoin.

### Et Dans quel but?

Le but est de construire tous ensemble un monde fantastique libre, cohérent et passionnant. C'est pour le moment carrément vide. Il ne tient qu'à nous de le remplir.

Héste pas à ouvrir une nouvelle [issue](https://gitlab.com/Daran/Diegese/issues) pour poser n'importe quelle question. De même, n'hésite pas à venir commenter les autres issues ouvertes. C'est à travers ton activité que ce monde existera. Toute participation intéressée est à prendre!

## Sommaire des parties à remplir

- Cosmogonie
    - Univers
- Cosmologie
    - Astres
    - Plans
- Panthéon
    - Dieux primordiaux
    - Dieux actuels
    - Dieux disparus ou morts
    - Acolytes divins emblématiques
- Légendes // non divine 
- Créatures/Bestiaire
    - Animaux
    - Monstres
- Races
    - Standard
    - Extraordinaires (élémentales, divines, etc.)
- Personnages emblématiques
    - Héros
- Organisations
    - Guilde
    - Ordre religieux
    - Royaumes
- Histoire
    - Ages
- Géographie
    - Environnements
    - Continents

Les documents que vous lirez ici sont ceux recueilli par les prêtres de Oghma. Certains documents sont contradictoires ou remis en question par des clergés alternatifs ou extérieurs.
Les avis divergents seront classés dans les groupuscules liés.